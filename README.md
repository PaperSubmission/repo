Improving Digital Twin Experience Reports
==========================

This repository stores a spreadsheet recording the application of our framework to digital twin experience reports selected from the literature.

The spreadsheet is available in both PDF and ODS file formats. ODS files can be opened with [LibreOffice](https://www.libreoffice.org/).

The characteristics of nineteen experience reports are presented here. Unclear information is highlighted in yellow, while missing information is highlighted in red. A suggested classification and the classification by Kritzinger *et al.* and Fuller *et al.* are at the bottom of the spreadsheet.

Seven experience reports were sourced from Kritzinger *et al.*, and six from Fuller *et al.*. Note that further papers were labelled as case studies in Kritzinger *et al.* and Fuller *et al.*, but from our reading these papers did not present a strong experience report and are therefore not expressed through our framework.

The goal is to demonstrate the applicability of our framework to DT solutions in multiple domains, and highlight how the framework provides a structure to ensure that essential characteristics are reported. Any missing or unclear information can affect the ease of classification of the solution and thus hamper further research and insights. 

For example, in five cases the digital twin classification (digital model, digital shadow, and digital twin) suggested by the report differs from that determined by Kritzinger *et al.* and Fuller *et al.*. This confusion over classification comes from the uncertainity over some of the characteristics and capabilities of the DT solution found in the experience reports.


